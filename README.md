# Graph-Go

A simple program that allows the user to play Go on arbitary graphs.
At the moment the implementation includes:

1) Square Lattice 

2) Triagonal Lattice

3) Hexagonal Lattice

4) Graphene Lattice

5) Kagome Lattice

6) Sierpinski Lattice

To start the program run:
julia Graph-Go.jl

Julia binaries can be downloaded at https://julialang.org/

NB: If you have never used julia before it will ask you to download some external packages (and thell you exaclty what to write in the terminal)