println("Including Misc.jl")

function GetNonZeroElements(Lattice,Offset=(0,0))
    ####Create a lsit with the non-zero elemets of the lattice
    (Ox,Oy)=Offset
    (Nx,Ny)=size(Lattice)
    NCount=sum(Lattice)
    #println("(Nx,Ny)=($Nx,$Ny)")
    #println("NCount=$NCount")
    IndVecMap=zeros(Int64,2,NCount) ###Numbers here need to be larger than the lattice size
    indx=1
    for ix in 1:Nx
        for iy in 1:Ny
            #println("($ix,$iy)="*string(Lattice[ix,iy]))
            if Lattice[ix,iy]
                IndVecMap[1,indx]=ix-1-Ox
                IndVecMap[2,indx]=iy-1-Oy
                indx+=1
            end
        end
    end
    return IndVecMap
end


function VIM_from_IVM(IVM)
    ###Find all unique numbers,sorted
    MaxRow=maximum(IVM[1,:])
    MaxCol=maximum(IVM[2,:])
    MinRow=minimum(IVM[1,:])
    MinCol=minimum(IVM[2,:])

    VIMap = fill(0,MaxRow-MinRow+1,MaxCol-MinCol+1)
    for Elem in 1:(size(IVM)[2])
        Row,Col=IVM[:,Elem]
        #println("Row,Col:$Row,$Col")
        VIMap[Row-MinRow+1,Col-MinCol+1]=Elem
    end
    #println("MinRow,MinCol:$MinRow,$MinCol")
    return (VIMap,(MinRow,MinCol))
end

function IsAnElement(Target,VIM)
    (TRow,TCol)=Target
    #println("Target=$Target")
    (VIMap,(MinRow,MinCol))=VIM
    (MaxRow,MaxCol)=size(VIMap).+(MinRow-1,MinCol-1)
    if MinRow>TRow || MinCol>TCol
        return nothing
    elseif MaxRow<TRow || MaxCol<TCol
        return nothing
    else
        Value = VIMap[TRow-MinRow+1,TCol-MinCol+1]
        if Value == 0
            return nothing
        else
            return Value
        end
    end
end

println("Leaving Misc.jl")
