import pygame
import sys
import numpy as np

import time
import pickle
import os


def GetSquareBoard(NX,NY):
    return jl.eval("BT=SquareGoBoard("+str(NX)+","+str(NY)+")")
    

from julia.api import Julia
jl = Julia(compiled_modules=False)   
jl.eval('include("BoardTypes.jl")')
jl.eval('include("BoardActions.jl")')
NX = 3
NY = 10


BT=GetSquareBoard(NX,NY)



pygame.init()
PixWidth = 600
PixHeight = 600

PlotWidth = 1.0
PlotHeight = 1.0


DISPLAYSURF = pygame.display.set_mode((PixWidth,PixHeight))


BLACK = pygame.Color(0, 0, 0)
L_BLACK = pygame.Color(80, 80, 80)
GREY = pygame.Color(150, 150, 150)
WHITE = pygame.Color(255, 255, 255)
L_WHITE = pygame.Color(175, 175, 175)
RED = pygame.Color(255, 0, 0)
BLUE = pygame.Color(0,0 , 255)
GREEN = pygame.Color(0, 255, 0)        

FPS = pygame.time.Clock()

LargeFont = pygame.font.SysFont('Comic Sans MS', 30)
SmallFont = pygame.font.SysFont('Comic Sans MS', 20)

ScreenNo = 0 ###Start with the start screen





def PosToPix(Pos):
    ####We assume the plot has the origin center, and then we build the other things around it
    XPos=np.real(Pos)
    YPos=np.imag(Pos)
    XPix =  np.int(np.round(PixWidth * (XPos/PlotWidth)))
    YPix =  np.int(np.round(PixHeight * (PlotHeight-YPos)/PlotHeight))
    return XPix,YPix

def PixToPos(XPix,YPix):
    ####We assume the plot has the origin center, and then we build the other things around it
    XPos =  PlotWidth * (XPix/PixWidth)
    YPos =  PlotHeight * (PixHeight-YPix)/PixHeight
    return XPos + 1j*YPos


def ScaleToPix(Scale):
    ####We assume the plot has the origin center, and then we build the other things around it
    return np.int(np.ceil(PixWidth*Scale/PlotWidth))


def DrawCircle(Pos,CircSize,Color,lw=None):
    PixPos=PosToPix(Pos)
    #print("Vec:",Vec)
    PixSize = ScaleToPix(CircSize)
    #print("Col:",Col)
    if lw == None:
        pygame.draw.circle(DISPLAYSURF, Color, PixPos, PixSize)
    else:
        pygame.draw.circle(DISPLAYSURF, Color, PixPos, PixSize,ScaleToPix(lw))


def GetMousePos():
    MX,MY=pygame.mouse.get_pos()
    return PixToPos(MX,MY)
    

def DrawLine(FromPos,ToPos,width,Color):
    FromPix=PosToPix(FromPos)
    ToPix=PosToPix(ToPos)
    PixWidth = ScaleToPix(width)
    pygame.draw.line(DISPLAYSURF, Color, FromPix, ToPix, PixWidth)


def TryAddStone(StoneID,BT):
    MoveAllowed,BT=jl.eval("MoveAllowed,BT=Do_AddStoneID("+
                           str(StoneID+1)+",BT)")
    return MoveAllowed,BT

    
NodeList = BT["IPM"] 

StoneSize=BT["StoneSize"]
NodeSize=StoneSize/3.
LineWidth=StoneSize/6.


while True:
    Turn=BT["StoneType"]

    #print("Lattice:",Lattice)

    
    
    for event in pygame.event.get():
        #print("event: ",event)
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            #print(event)
            if event.key == pygame.K_q: ###Quit
                pygame.quit()
                sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            MousePos=GetMousePos()
            for indx in range(BT["NumNodes"]):
                NodePos = NodeList[indx]
                if np.abs(MousePos-NodePos) < StoneSize:
                    MoveAllowed,BT=TryAddStone(indx,BT)
                    if MoveAllowed:
                        break
                    else:
                        ###Flash red since it's forbidden
                        DISPLAYSURF.fill(RED)
                        pygame.display.update()
                        FPS.tick(10)

    DISPLAYSURF.fill(GREY)

    LinkMap=BT["LinkMap"]
    #print("LinkMap:",LinkMap)
    for indx in range(np.shape(LinkMap)[0]):
        FPos=LinkMap[indx,0]
        TPos=LinkMap[indx,1]
        DrawLine(FPos,TPos,LineWidth,BLACK)

    
    MousePos=GetMousePos()


    Lattice=BT["BoardState"]
    for indx in range(BT["NumNodes"]):
        NodePos = NodeList[indx]
        if Lattice[indx] == -1:
            DrawCircle(NodePos,StoneSize,BLACK)
        elif Lattice[indx] == +1:
            DrawCircle(NodePos,StoneSize,WHITE)
        elif Lattice[indx] == 0:            
            DrawCircle(NodePos,NodeSize,BLUE)
            if np.abs(MousePos-NodePos) < StoneSize:
                if Turn==-1:
                    DrawCircle(NodePos,StoneSize,L_BLACK)
                elif Turn==+1:
                    DrawCircle(NodePos,StoneSize,L_WHITE)            
    #DrawCircle(MousePos,NodeSize,RED)
    if len(BT["History"]) > 0:
        LastNodePos = NodeList[BT["History"][-1]-1]
        DrawCircle(LastNodePos,StoneSize*0.75,RED,lw=StoneSize*0.10)
                      
    pygame.display.update()
    FPS.tick(60)
