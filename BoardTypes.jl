

println("Including BoardTypes.jl")
include("Misc.jl")
include("SierpinskiBoard.jl")

using Test
#using SierpinskiBoard

function get_triagonal_neigbours(IVM)
    VIM=VIM_from_IVM(IVM)
    NumNodes=size(IVM)[2]
    ###Next we define which nodes are adjacent
    ###For this we use an array of arrays
    NeigborList=Array{Array{Int64, 1}}(undef,NumNodes)
    ###Now we go though the indvecmap and loook for neigburs
    for Node1 in 1:NumNodes
        #println(".............")
        #println("Node1=$Node1")
        NodePos=IVM[:,Node1]
        #println("NodePos=$NodePos")            
         ###There are four possible neihbours on the suquare lattice
        LocalNeigborList=fill(0,6)
        NeigburID=1
        for (dx,dy) in ((1,0),(-1,0),(0,1),(0,-1),(1,-1),(-1,1))
            NNodePos=NodePos .+ (dy,dx)
            #println("NNodePos=$NNodePos")            
            Node2 = IsAnElement(NNodePos,VIM)
            if Node2!=nothing                
                #println("Node2=$Node2")
                LocalNeigborList[NeigburID]=Node2
                NeigburID+=1
            end
        end
        NeigborList[Node1]=LocalNeigborList[1:(NeigburID-1)]
    end
    return NeigborList
end


function get_square_neigbours(IVM)
    VIM=VIM_from_IVM(IVM)
    NumNodes=size(IVM)[2]
    ###Next we define which nodes are adjacent
    ###For this we use an array of arrays
    NeigborList=Array{Array{Int64, 1}}(undef,NumNodes)
    ###Now we go though the indvecmap and loook for neigburs
    for Node1 in 1:NumNodes
        #println(".............")
        #println("Node1=$Node1")
        NodePos=IVM[:,Node1]
        #println("NodePos=$NodePos")            
        ###There are four possible neihbours on the suquare lattice
        LocalNeigborList=fill(0,4)
        NeigburID=1
        for (dx,dy) in ((1,0),(-1,0),(0,1),(0,-1))
            NNodePos=NodePos .+ (dy,dx)
            #println("NNodePos=$NNodePos")            
            Node2 = IsAnElement(NNodePos,VIM)
            if Node2!=nothing                
                #println("Node2=$Node2")
                LocalNeigborList[NeigburID]=Node2
                NeigburID+=1
            end
        end
        NeigborList[Node1]=LocalNeigborList[1:(NeigburID-1)]
    end
    return NeigborList
end

function WeedHoles(IVM,Holes::Integer)
    ###Weed out some holes
    SizeIVM=size(IVM)[2]
    if Holes >= SizeIVM || Holes < 0
        throw(DomainError(Holes,"The number of eleents to remove is not in the range 0:$(SizeIVM-1)"))
    end
    RandNum=rand(Holes)
    for RandID in 1:Holes
        DeleteID=Int(ceil(RandNum[RandID]*(SizeIVM-RandID+1)))
        IVM=hcat(IVM[:,1:(DeleteID-1)],IVM[:,(DeleteID+1:end)])
    end
    return IVM
end

function get_Sierpinski_IPM(IVM,LT,SizeGen) 
    #if LT =="SGR" || LT =="SGT"
    #    return IPM = IVM .* 1.0
    #end
    if LT[2]=='G' || LT[2]=='T'
        MaxRow=2^SizeGen
        IVMMOD=fill(0,size(IVM))
        IVMMOD[1,:]=MaxRow .- IVM[2,:]
        IVMMOD[2,:]=IVM[1,:]
        IVMMOD[1,:]=IVMMOD[1,:]-IVMMOD[2,:]
        IPM = get_triangular_IPM(IVMMOD)
    elseif LT[3]=='T'
        IPM = get_triangular_IPM(IVM)
    elseif LT[3]=='R'
        IPM = get_anti_triangular_IPM(IVM)
    elseif LT[3]=='X' || LT[3]=='S'
        IPM = IVM .* 1.0
    end
    return IPM
end
    

function get_triangular_IPM(IVM)
    IPM=fill(0.0,size(IVM))
    IPM[1,:]=IVM[1,:] .* sqrt(3.0/4.0)
    IPM[2,:]=IVM[2,:] .+ .5 .* IVM[1,:]
    return IPM
end

function get_anti_triangular_IPM(IVM)
    IPM=fill(0.0,size(IVM))
    IPM[1,:]=IVM[1,:] .* sqrt(3.0/4.0)
    IPM[2,:]=IVM[2,:] .- .5 .* IVM[1,:]
    return IPM
end


function SierpinskiGoBoard(SizeGen::Integer,FracGen::Integer,
                           LT::String="SCS",Holes::Integer=0)
    println("Create Sirpinski for SizeGen=$SizeGen, FracGen=$FracGen"*
            "LT=$LT, Holes=$Holes")
    IVM=GetSierpinskiIVM(SizeGen,FracGen,LT)
    IVM=WeedHoles(IVM,Holes)
    NeigborList=get_Sierpinski_neigbours(IVM,SizeGen,FracGen,LT)
    IPM=get_Sierpinski_IPM(IVM,LT,SizeGen)
    BoardData=InitiateBoardData(IPM,NeigborList)
    return BoardData
end

function KagomeGoBoard(Radius::Integer,Holes::Integer=0)
    Size=2*Radius+1
    ###This defined the lattice sites
    HasNode=fill(true,(Size,Size))   
    for XIndx in 1:div(Size,2)
        for YIndx in 1:div(Size,2)
            Nx=2*XIndx
            Ny=2*YIndx
            if ( Ny<=Size && Ny>0 ) && ( Nx<=Size && Nx>0 )
                HasNode[Ny,Nx]=false
            end
        end
    end
    IVM=GetNonZeroElements(HasNode)
    IVM=WeedHoles(IVM,Holes)
    NeigborList=get_triagonal_neigbours(IVM)
    ####compute the corresponding real space positions
    IPM=get_triangular_IPM(IVM)
    ###Iniate the board
    BoardData=InitiateBoardData(IPM,NeigborList)
    return BoardData
end


function GrapheneGoBoard(Radius::Integer,Holes::Integer=0)
    if Radius == 1
        Size=3
    elseif Radius == 2
        Size=7
    else
        Size=4*Radius-1
    end
    ###This defined the lattice sites
    HasNode=fill(true,(Size,Size))   

    for XIndx in 0:(Size-1)
        for YIndx in -div(Size,2):div(Size,2)
            Ny=2*YIndx+XIndx+1
            Nx=XIndx+1-YIndx
            if ( Ny<=Size && Ny>0 ) && ( Nx<=Size && Nx>0 )
                HasNode[Ny,Nx]=false
            end
        end
    end
    #### Next step we remove some of the spurious points


    LowCorner=Radius+2*(Radius-1)
    for IndxY in 0:(Radius-1)
        for IndxX in 0:(LowCorner-(2*IndxY)-1)
            Ny=IndxY+1
            Nx=IndxX+1
            HasNode[Ny,Nx]=false
            HasNode[Nx,Ny]=false
            HasNode[Size-Ny+1,Size-Nx+1]=false
            HasNode[Size-Nx+1,Size-Ny+1]=false

        end
    end
    for IndxX in (LowCorner+3):Size
        for IndxY in 0:(IndxX-LowCorner-3)
            Ny=IndxY+1
            Nx=IndxX
            HasNode[Ny,Nx]=false
            HasNode[Size-Ny+1,Size-Nx+1]=false
        end
    end

    IVM=GetNonZeroElements(HasNode)
    IVM=WeedHoles(IVM,Holes)
    NeigborList=get_triagonal_neigbours(IVM)
    ####compute the corresponding real space positions
    IPM=get_triangular_IPM(IVM)
    ###Iniate the board
    BoardData=InitiateBoardData(IPM,NeigborList)
    return BoardData
end



function HexagonGoBoard(Radius::Integer,Holes::Integer=0)
    Size=Radius*2-1
    ###This defined the lattice sites
    HasNode=fill(true,(Size,Size))   

    for Nx in 1:(Radius-1)
        for Ny in 1:(Radius-1-Nx+1)
            HasNode[Ny,Nx]=false
            HasNode[Size-Ny+1,Size-Nx+1]=false
        end
    end
    
    IVM=GetNonZeroElements(HasNode)
    IVM=WeedHoles(IVM,Holes)
    NeigborList=get_triagonal_neigbours(IVM)
    ####compute the corresponding real space positions
    IPM=get_triangular_IPM(IVM)
    ###Iniate the board
    BoardData=InitiateBoardData(IPM,NeigborList)
    return BoardData
end




function TriangleGoBoard(Size,Holes=0)
    ###This defined the lattice sites
    HasNode=fill(true,(Size,Size))   
    for Nx in 1:Size
        for Ny in (Size-Nx+2):Size
            HasNode[Nx,Ny]=false
        end
    end

    IVM=GetNonZeroElements(HasNode)
    IVM=WeedHoles(IVM,Holes)
    NeigborList=get_triagonal_neigbours(IVM)
    ####compute the corresponding real space positions
    IPM=get_triangular_IPM(IVM)
    ###Iniate the board
    BoardData=InitiateBoardData(IPM,NeigborList)
    return BoardData
end



function SquareGoBoard(Nx,Ny,Holes=0)
    ###This defined the lattice sites
    HasNode=fill(true,(Nx,Ny))

    IVM=GetNonZeroElements(HasNode)
    IVM=WeedHoles(IVM,Holes)
    NeigborList=get_square_neigbours(IVM)
    ####compute the corresponding real space positions
    IPM=IVM[1,:] .+ im.*IVM[2,:]
    ###Iniate the board
    BoardData=InitiateBoardData(IPM,NeigborList)
    return BoardData
end

function recenter_IPM(IPM,LowLeft,UppRight)
    ####Figure out the closest distance between any points

    MinDist=0
    for indx1 in 1:(length(IPM)-1)
        for indx2 in (indx1+1):length(IPM)
            if indx1==1 && indx2==2
                MinDist=abs(IPM[1]-IPM[2])
            else
                MinDist=min(MinDist,abs(IPM[indx1]-IPM[indx2]))
            end
        end
    end
    println("MinDist: ",MinDist)
    StoneSize=MinDist/2 ###Stones should just touch,,,

    TargetSizeX=real(UppRight-LowLeft)
    TargetSizeY=imag(UppRight-LowLeft)
    NewCenter=.5*(UppRight+LowLeft)
    println("NewCenter: ",NewCenter)
    
    ##Find the bounds of the lattice
    MinX=minimum(real.(IPM))-StoneSize
    MinY=minimum(imag.(IPM))-StoneSize
    MaxX=maximum(real.(IPM))+StoneSize
    MaxY=maximum(imag.(IPM))+StoneSize
    ScaleX = MaxX - MinX 
    ScaleY = MaxY - MinY 
    MeanX = .5*(MaxX + MinX)
    MeanY = .5*(MaxY + MinY)
    ###We want the scale to be choose such that the lattice uppon a rescaling anf zoooming precicely fits in the desited box.

    ###We begin by making the scales match
    minScale=min(TargetSizeX/ScaleX,TargetSizeY/ScaleY)

    IPM = minScale.*(IPM .- MeanX .- im*MeanY) .+ NewCenter
    return IPM, StoneSize*minScale    
end


function InitiateBoardData(IPM,NeigborList)
    IPM,StoneSize = recenter_IPM(IPM,0.1+0.1*im,0.9+0.9*im)    
    Prisoners=fill(0,2)
    NumNodes=length(NeigborList)
    BoardState=fill(0,NumNodes)
    ClusterID=fill(0,NumNodes)
    LinkMap=get_link_map(IPM,NeigborList)
    ##We use integers to map to arrays
    ClusterDict=Dict{Integer,Array{Int64,1}}()
    AreaDict=Dict{Integer,Array{Int64,1}}()
    AreaOwner=Dict{Integer,Int64}()
    BoardData=Dict("IPM"=>IPM,
                   "StoneSize"=>StoneSize,
                   "NeigborList"=>NeigborList,
                   "LinkMap"=>LinkMap,
                   "NextClusterID"=>1,
                   "NextAreaID"=>-1,
                   "NumNodes"=>NumNodes,
                   "ClusterID"=>ClusterID,
                   "ClusterDict"=>ClusterDict,
                   "AreaDict"=>AreaDict,
                   "AreaOwner"=>AreaOwner,
                   "BoardState"=>BoardState,
                   "Prisoners"=>Prisoners,
                   "History"=>fill(0,0),
                   "StoneType"=>1,
                   "AlwaysScore"=>false)
    return BoardData
end



function get_link_map(IPM,NeigborList)
    ###Loop through and count elements
    NumElems=0
    for ID in 1:length(NeigborList)
        for NBIN in NeigborList[ID]
            if NBIN > ID
                NumElems+=1
            end
        end
    end
    ###Now we construct the map
    LinkMap=fill(0.0im,NumElems,2)
    NumElems=0
    for ID in 1:length(NeigborList)
        IDPos=IPM[ID]
        #println(NeigborList[ID])
        for NBID in NeigborList[ID]
            if NBID > ID
                NumElems+=1
                NBIDPos=IPM[NBID]
                LinkMap[NumElems,:] = [IDPos,NBIDPos]
            end
        end
    end
    return LinkMap
end


println("Exiting BoardTypes.jl")






