#### In this file we will begin to look at the game of go played on a generic graph.
#### We will begin with a square lattice, but we can add some defencts.


function Enter()
    print("Press Enter")
    inp=readline(stdin)
end

####Initialize the first board
using PyPlot
#figure()
#Enter()


include("BoardTypes.jl")
include("BoardPlotting.jl")
include("BoardActions.jl")


function PlayTheGame(BoardData;Players=nothing)
    #return
    if Players == nothing
        while true
            println("(1) One player of (2) Two player game!")
            print("1 or 2:")
            inp=readline(stdin)
            if inp=="1"
                BoardData["SinglePlayer"]=true
                break
            elseif inp=="2"
                BoardData["SinglePlayer"]=false
                break
            else
                println("What number of players?")
            end
        end
    elseif Players == 1
        BoardData["SinglePlayer"]=true
    elseif Players == 2
        BoardData["SinglePlayer"]=false
    else
        error("Number of playes = $Players is not 1 or 2")
    end

    figure() ###FIXME workaround - creating the figure with PyPlot
    ###For some reason PyCall:s initialization is broken and only a black window is created

    PlotBoard(BoardData)
    ConnectorID=py"ClickCollector"(plt.gcf(),BoardData)
    ToggleScoreIDConnector(ConnectorID)

    
    println("Enter \"q\" to quit!")
    println("Enter \"p\" to pause!")
    println("Enter \"s\" to trigger scoring!")
    println("Enter \"S\" to toggle automatic Scoring!")
    println("Enter \"r\" for a random move")
    println("Enter \"ID\" to place stone at Node=ID")
    println("Enter \"x y\" to make a move")
    println("or Click on the figure!")
    
    DoScoring=false
    Stones=20
    while true
        print("Choose action:")
        if false
            if Stones > 0
                inp="r"
                Stones-=1
            elseif Stones == 0
                Stones-=1
                inp="E"
            else
                inp=readline(stdin)
            end
        else
            inp=readline(stdin)
        end
        if inp=="q"
            println("quiting!")
            break
        elseif inp=="E"
            println("End of Game scoring!")
            DoScoring=true
            break
        elseif inp=="S"
            println("Toggle the Score function")
            ToggleScoreIDConnector(ConnectorID)
            GameDict=PyDict(ConnectorID["bd"])
            ScoreTheGame(GameDict)
        elseif inp=="s"
            println("scoring time!")
            GameDict=PyDict(ConnectorID["bd"])
            ScoreTheGame(GameDict)
        elseif inp=="r"
            println("Choosing random position")
            AddRandomStoneIDConnector(ConnectorID)
        elseif inp=="P" || inp=="p" || inp=="Pass" || inp=="pass"
            println("Choosing to pass")
            PassIDConnector(ConnectorID)
        else
            StringSplit=split(inp)
            if length(StringSplit)==1
                PyBoardDict=PyDict(ConnectorID."bd")                
                TP=tryparse(Int,StringSplit[1])
                if TP!=nothing
                    BoardState=PyBoardDict["BoardState"]
                    NumNodes=PyBoardDict["NumNodes"]
                    if TP < 1 || TP > NumNodes
                        println("!! ID is not in range 1<=ID<=$NumNodes !!")
                    else
                        TryID=TP
                        AddStoneIDConnector(TryID,ConnectorID)
                    end
                else
                    println("?? What position ID ??")
                end
            elseif length(StringSplit)==2
                PyBoardDict=PyDict(ConnectorID["bd"])
                TP1=tryparse(Float64,StringSplit[1])
                TP2=tryparse(Float64,StringSplit[2])
                if (TP1 !=nothing) && (TP2 !=nothing)
                    AddStonePosConnector((TP2,TP1),ConnectorID)
                else
                    println("?? What position ??")
                end
            end
        end
    end
    GameDict=PyDict(ConnectorID."bd")
    plt.gcf()."canvas"."mpl_disconnect"(ConnectorID."cid")
    if DoScoring
        println("End Game Scoring: Klick on dead stones. Press \"D\" then done.")
        ConnectorID=py"ClickRemover"(plt.gcf(),GameDict)
        while true 
            inp=readline(stdin)
            if inp=="D"
                break
            end
        end
        GameDict=PyDict(ConnectorID["bd"])
        plt.gcf()."canvas"."mpl_disconnect"(ConnectorID."cid")
        return ScoreTheGame(GameDict)
    else
        return GameDict
    end
end

function get_integers(TargetList)
    NumInts=length(TargetList)
    IntsPresent=0
    IntList=fill(0,NumInts)
    while true
        for TargetNo in (IntsPresent+1):NumInts
        println("Specify $(TargetList[TargetNo]):")
        end
        print("Specify:")
        inp=readline(stdin)
        StringSplit=split(inp)
        #println("Split Input:")
        #display(StringSplit)
        for Strings in StringSplit
            TP=tryparse(Int,Strings)
            if TP!=nothing
                IntsPresent+=1
                IntList[IntsPresent]=TP
                if IntsPresent==NumInts
                    println("Returning IntList=$IntList")
                    return IntList
                end
            else
                ###Input missunderstood try again
                break
            end
        end
    end
end


function get_strings(TargetList)
    NumInts=length(TargetList)
    IntsPresent=0
    StringList=fill("",NumInts)
    while true
        for TargetNo in (IntsPresent+1):NumInts
        println("Specify $(TargetList[TargetNo]):")
        end
        print("Specify:")
        inp=readline(stdin)
        StringSplit=split(inp)
        for Strings in StringSplit
            IntsPresent+=1
            StringList[IntsPresent]=Strings
            if IntsPresent==NumInts
                println("Returning StringList=$StringList")
                return StringList
            end
        end
    end
end


function Choose_Game()
    while true
        println("Choose which type of Go Board Lattice to use:")
        println("1) Square Lattice ")
        println("2) Triagonal Lattice")
        println("3) Hexagonal Lattice")
        println("4) Graphene Lattice")
        println("5) Kagome Lattice")
        println("6) Sierpinski Lattice")
        println("Q) Quit")
        
        print("Choose:")
        inp=readline(stdin)
        if inp=="1"
            IntList=get_integers(["width","height","# of missing nodes"])
            BoardData=SquareGoBoard(IntList[1],IntList[2],IntList[3])
        elseif inp=="2"
            IntList=get_integers(["Size","# of missing nodes"])
            BoardData=TriangleGoBoard(IntList[1],IntList[2])
        elseif inp=="3"
            IntList=get_integers(["Size","# of missing nodes"])
            BoardData=HexagonGoBoard(IntList[1],IntList[2])
        elseif inp=="4"
            IntList=get_integers(["Size","# of missing nodes"])
            BoardData=GrapheneGoBoard(IntList[1],IntList[2])
        elseif inp=="5"
            IntList=get_integers(["Size","# of missing nodes"])
            BoardData=KagomeGoBoard(IntList[1],IntList[2])
        elseif inp=="6"
            println("There are many types of Sierpinsky Lattices")
            println("They have the name S{G}{L} where")
            println("G={C=carpet,N=Net,G=gasket,T=triangle}")
            println("L={S=Square,T=Tiangle,R=Reverse-Triangle,X=[TR]=X-links}")
            LT=get_strings(["Lattice ID = S{G}{L}"])[1]
            validate_LT(LT)
            if LT[2]=='C'
                println("System size grows as: 3^SizeGeneration")
                println("FractionGeneration<=SizeGeneration")
            elseif LT[2]=='N'
                println("System size grows as: 3^SizeGeneration")
                println("FractionGeneration<=SizeGeneration")
            elseif LT[2]=='T'
                println("System size grows as: 2^SizeGeneration")
                println("FractionGeneration < SizeGeneration")
            elseif LT[2]=='G'
                println("System size grows as: 2^SizeGeneration")
                println("FractionGeneration < SizeGeneration")
            end
            IntList=get_integers(["SizeGeneration","FractionGeneration","# of missing nodes"])
            BoardData=SierpinskiGoBoard(IntList[1],IntList[2],LT,IntList[3])
        elseif inp=="Q"
            println("Quitting: See you another time")
            break
        end
    end
end




#SierpinskiGoBoard(5,4,"STT",0)
#SquareGoBoard(5,4,2;Players=1)
#GrapheneGoBoard(4,0)

Choose_Game()
