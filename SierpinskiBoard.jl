#module SierpinskiBoard

#export GetSierpinskiIVM,get_Sierpinski_neigbours,validate_LT

println("Including SierpinskiBoard.jl")
include("Misc.jl")


###This file defines the functions nececarry to diagponalise the serpinsly carpet, gaskes and triangle....

function get_errtext()
    return "Only LatticeType=S{C=carpet,N=Net,G=gasket,T=triangle}{S=Square,T=Tiangle,R=Reverse-Triangle,X=[TR]=X-hopping} are supported"
end


function validate_LT(LT)
    if LT[1]!='S' || length(LT)!=3
        throw(DomainError(LT,get_errtext()))
    end
    if LT[2]!='C' && LT[2]!='T' && LT[2]!='G' && LT[2]!='N'
        throw(DomainError(LT,get_errtext()))
    end
    if LT[3]!='S' && LT[3]!='T' && LT[3]!='R' && LT[3]!='X'
        throw(DomainError(LT,get_errtext()))
    end
end

function get_lattice_size(SizeGen,LT)
    validate_LT(LT)
    if SizeGen<0
        Text="SizeGen=$SizeGen has to be >= 0!"
        throw(DomainError(SizeGen,Text))
    end
    if LT[2]=='C'  ###Carpet
        return 3^SizeGen
    elseif LT[2]=='T' ###Triangle
        return 2^SizeGen
    elseif LT[2]=='G' ###Gascet
        return 2^SizeGen+1
    elseif LT[2]=='N'  ###Net
        if SizeGen==1
            return 3
        elseif SizeGen==0
            return 1
        else
            return get_lattice_size(SizeGen-1,LT)*3-2
        end
    end
end


function is_bond_allowed(row1,col1,row2,col2,BondType::Char)
    DiffRow=abs(row1-row2)
    DiffCol=abs(col1-col2)
    if ( DiffRow==0 && DiffCol==1 ) || ( DiffRow==1 && DiffCol==0 ) ##Square lattice
        return true
    elseif (row1-row2 == 1) && (col1-col2 == 1)  || (row1-row2 == -1) && (col1-col2 == -1)
        if BondType == 'R' || BondType == 'X'
            return true
        else
            return false
        end
    elseif (row1-row2 == 1) && (col1-col2 == -1)  || (row1-row2 == 1) && (col1-col2 == -1)
        if BondType == 'T' || BondType == 'X'
            return true
        else
            return false
        end
    else
        throw(DomainError("FIXME: Reached unknown point"))
    end
end

function bond_is_present(LT,SizeGen,FracGen,(row1,col1),(row2,col2))
    if col2 < col1 || (col2==col1 && row2<row1)
        ###Order such that we onlyce consider hopping from left to right or down to up
        return bond_is_present(LT,SizeGen,FracGen,(row2,col2),(row1,col1))
    elseif abs(row1-row2) > 1 || abs(col1-col2) > 1
        ###Allow only nearest neighbors or next nearest neighbors
        return false
    else
        if LT[2] == 'C' || LT[2]=='T' || LT[2]=='H'
            ###Carpet and triangle and hexagon are defined in terms of their lattice points alone
            return is_bond_allowed(row1,col1,row2,col2,LT[3])
        elseif LT[2] == 'G'
            if !is_bond_allowed(row1,col1,row2,col2,LT[3])
                return false ###The bond has to be at least allowed
            end
            ###For the gascet there is on level 1 a special case
            if FracGen==0
                return true
            else
                SideSize=2^(SizeGen-1)
                println("SideSize: $SideSize")
                println("SizeGen,FracGen: $SizeGen, $FracGen")
                println("(row1,col1)=($row1,$col1)")
                println("(row2,col2)=($row2,$col2)")
                if SizeGen==1
                    return true
                elseif  SizeGen>=2
                    if ((row1,col1) == (0, SideSize-1) &&
                        (row2,col2) == (0, SideSize)) ##Horizontal ok
                        return true
                    elseif ((row1,col1) == (SideSize-1,0) &&
                            (row2,col2) == (SideSize,0)) ##Vertical ok
                        return true
                    elseif ((row1,col1) == (1, SideSize-1) &&
                            (row2,col2) == (1, SideSize)) ##Horizontal+1 not ok
                        return false
                    elseif ((row1,col1) == (SideSize-1,1) &&
                            (row2,col2) == (SideSize,1)) ##Vertical+1 not ok
                        return false
                    elseif ((row1,col1) == (0, SideSize-1) &&
                            (row2,col2) == (1, SideSize)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (SideSize-1,0) &&
                            (row2,col2) == (SideSize,1)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (SideSize-1,1) &&
                            (row2,col2) == (SideSize,2)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (1, SideSize-1) &&
                            (row2,col2) == (2, SideSize)) ### "up/right"
                        return false ##not ok
                    elseif ((row1,col1) == (1, SideSize-1) &&
                            (row2,col2) == (0, SideSize)) ### "down/right"
                        return true ## ok
                    elseif ((row1,col1) == (SideSize-1,1) &&
                            (row2,col2) == (SideSize,2)) ### "down/right"
                        return true ## ok
                    elseif ((row1,col1) == (SideSize,SideSize-1) &&
                            (row2,col2) == (SideSize-1,SideSize)) ### "down/right
                        return false ## ok
                    elseif col1>= SideSize
                        return bond_is_present(LT,SizeGen-1,FracGen-1,
                                               (row1,col1-SideSize),(row2,col2-SideSize))
                    elseif row1>=SideSize
                        return bond_is_present(LT,SizeGen-1,FracGen-1,
                                               (row1-SideSize,col1),(row2-SideSize,col2))
                    elseif row1 < SideSize && col1 < SideSize && row2 < SideSize && col2 < SideSize
                        return bond_is_present(LT,SizeGen-1,FracGen-1,(row1,col1),(row2,col2))
                    end
                end
            end
        end
    end
    return false
end


function GetSierpinskiIVM(SizeGen::Integer,FracGen::Integer,LT::String)
    validate_LT(LT)
    N=get_lattice_size(SizeGen,LT)
    ####Begin by building the lattice####
    TheLattice=GetTheLattice(SizeGen,FracGen,LT) ###Plot if exists
    IndVecMap=GetNonZeroElements(TheLattice) ###Ellemtns, ordered after row and then column
    return IndVecMap
end

function GetSierpinskyNeighbours(row::Integer,col::Integer,LT::String)
    if LT[3]=='T' ### Triangular lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col),#up
                (row+1,col-1),#(back/up)
                (row-1,col+1))#(forw/down)
    elseif LT[3]=='R' ### Anti-Triangular lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col),#up
                (row+1,col+1),#(back/up)
                (row-1,col-1))#(forw/down)
    elseif LT[3]=='X'  ### X- lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col),#up
                (row+1,col-1),#(back/up)
                (row-1,col+1),#(forw/down)
                (row+1,col+1),#(forw/up)
                (row-1,col-1))#(back/down)
    elseif LT[3]=='S'### Square lattice
        return ((row,col-1), #backward
                (row,col+1),#forward
                (row-1,col),#down
                (row+1,col))#up
    else
        throw(DomainError(LT,"Unknown Lattice"))
    end
end

function get_Sierpinski_neigbours(IVM,SizeGen::Integer,FracGen::Integer,LT::String)

    VIM=VIM_from_IVM(IVM)
    NumNodes=size(IVM)[2]
    ###Next we define which nodes are adjacent
    ###For this we use an array of arrays
    NeigborList=Array{Array{Int64, 1}}(undef,NumNodes)
    ###Now we go though the indvecmap and loook for neigburs
    for Node1 in 1:NumNodes
        #println(".............")
        #println("Node1=$Node1")
        NodePos=IVM[:,Node1]
        #println("NodePos=$NodePos")            
        NeigburID=1
        (row,col)=NodePos
        SerpNeigh=GetSierpinskyNeighbours(row,col,LT)
        ##The maximum amoun of niegbbours is given by SerpNeigh
        LocalNeigborList=fill(0,length(SerpNeigh))
        for NNodePos in SerpNeigh
            #println("NNodePos=$NNodePos")            
            Node2 = IsAnElement(NNodePos,VIM)
            if Node2!=nothing
                if  bond_is_present(LT,SizeGen,FracGen,NodePos,NNodePos)
                    #println("Node2=$Node2")
                    LocalNeigborList[NeigburID]=Node2
                    NeigburID+=1
                end                        
            end
        end
        NeigborList[Node1]=LocalNeigborList[1:(NeigburID-1)]
    end
    return NeigborList
end


function GetTheLattice(SizeGen,Fracgen,LT)
    N=get_lattice_size(SizeGen,LT)
    LatticeMatrix=zeros(Bool,N,N)
    for col in 0:(N-1)
        LatticeMatrix[col+1,:]=GetLatticeSlice(SizeGen,Fracgen,LT,col)
    end
    return LatticeMatrix
end

function GetLatticeSlice(SizeGen,Fracgen,LT,col)
    N=get_lattice_size(SizeGen,LT)
    LatticeMatrixSlize=zeros(Bool,N)
    for row in 0:(N-1)
        LatticeMatrixSlize[row+1]=IsOnFractal(row,col,SizeGen,Fracgen,LT)
    end
    return LatticeMatrixSlize
end

function IsOnFractal(row,col,SizeGen,FracGen,LT,verb=false)
    #println("-------------------------")
    if SizeGen<0
        Text="SizeGen=$SizeGen has to be >= 0!"
        throw(DomainError(SizeGen,Text))
    end
    if FracGen<0
        Text="FracGen=$FracGen has to be >= 0!"
        throw(DomainError(FracGen,Text))
    end
    if LT[2]=='C'
        IsOnSCFractal(row,col,SizeGen,FracGen,verb)
    elseif LT[2]=='N'
        IsOnSNFractal(row,col,SizeGen,FracGen,verb)
    elseif LT[2]=='T'
        IsOnSTFractal(row,col,SizeGen,FracGen,verb)
    elseif LT[2]=='G'
        IsOnSGFractal(row,col,SizeGen,FracGen,verb)
    else
        throw(DomainError(LT,get_errtext()))
    end
end

function IsOnSGFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<2 && FracGen>0
        Text="There is no nontrivial FracGen!=0 for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    elseif SizeGen>=2 && FracGen==(SizeGen-1)
        IsOnSGFractal(row,col,SizeGen,FracGen-1,verb)
    elseif SizeGen>=2 && FracGen>(SizeGen-1)
        Text="There is no nontrivial FracGen>"*string(SizeGen-1)*" for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    ###Determine if a point if on the fractal
    EffSize=2^SizeGen
    ###We first want to define a triangle of exclusion (this is independent of the FracGen)
    verb && println("($row,$col) of SizeGen $SizeGen : #$EffSize")
    if row+col>EffSize
        return false
    elseif FracGen==0
        return true
    else
        ###First we determin which half this is in
        rowpart=div(row,2^(SizeGen-1))
        colpart=div(col,2^(SizeGen-1))
        rowdiff=rem(row,2^(SizeGen-1))
        coldiff=rem(col,2^(SizeGen-1))
        verb && println("($rowpart,$colpart)")
        if !(rowpart==1 && colpart==1)
            verb && println("On this generation of the lattice")
            if SizeGen>1
                verb && println("($rowdiff,$coldiff)")
                return IsOnSGFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
            else
                return true
            end
        else
            if rowdiff==0 && coldiff==0
                return true
            else
                return false
            end
        end
    end
end


function IsOnSTFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<2 && FracGen>0
        Text="There is no nontrivial FracGen!=0 for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    elseif SizeGen>=2 && FracGen>(SizeGen-1)
        Text="There is no nontrivial FracGen>"*string(SizeGen-1)*" for SizeGen=$SizeGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    ###Determine if a point if on the fractal
    EffSize=2^SizeGen
    ###We first want to define a triangle of exclusion (this is independent of the FracGen)
    verb && println("..........................")
    verb && println("($row,$col) of SizeGen=$SizeGen, FracGen=$FracGen : #$EffSize")
    if row+col>(EffSize-1)
        return false
    elseif FracGen==0
        return true
    else
        ###First we determin which half this is in
        rowpart=div(row,2^(SizeGen-1))
        colpart=div(col,2^(SizeGen-1))
        rowdiff=rem(row,2^(SizeGen-1))
        coldiff=rem(col,2^(SizeGen-1))
        verb && println("quandrant ($rowpart,$colpart)")
        if !(rowpart==1 && colpart==1)
            verb && println("On this generation of the lattice")
            if SizeGen>1
                verb && println("($rowdiff,$coldiff)")
                verb && println(".,.,.,.,.,.,.,.,")
                return IsOnSTFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
            else
                return true
            end
        else
            return false
        end
    end
end



function IsOnSCFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<FracGen
        Text="SizeGen=$SizeGen has to be bigger or equal to FracGen=$FracGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    FracGen==0 && return true
    ###Determine if a point if on the fractal
    EffSize=3^SizeGen
    verb && println("($row,$col) of SizeGen $SizeGen : #$EffSize")
    ###First we determin which third this is in
    rowpart=div(row,3^(SizeGen-1))
    colpart=div(col,3^(SizeGen-1))
    verb && println("($rowpart,$colpart)")
    if !(rowpart==1 && colpart==1)
        verb && println("On this generation of the lattice")
        if SizeGen>1
            rowdiff=rem(row,3^(SizeGen-1))
            coldiff=rem(col,3^(SizeGen-1))
            verb && println("($rowdiff,$coldiff)")
            return IsOnSCFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
        else
            return true
        end
    else
        return false
    end
end


function IsOnSNFractal(row,col,SizeGen,FracGen,verb=false)
    if SizeGen<FracGen
        Text="SizeGen=$SizeGen has to be bigger or equal to FracGen=$FracGen"*"!"
        throw(DomainError(FracGen,Text))
    end
    ###Determine if a point if on the fractal
    FullSize=get_lattice_size(SizeGen,"SNS")
    
    verb && println("($row,$col) of SizeGen $SizeGen : #$FullSize")
    if row >= FullSize || col >= FullSize || row < 0 || col < 0
        verb && println("Outside")
        return false
    elseif FracGen==0
        verb && println("FracGen==0")
        return true
    elseif row == (FullSize-1) || col == (FullSize-1) || row == 0 || col == 0
        verb && println("on Boundary")
        return true
    else ###Now you are on the inner squares
        if SizeGen==0 ###Should be onlyone lelemnts
            verb && println("SizeGen==0")
            return true
        elseif SizeGen==1 ###Should be onlyone lelemnts
            if (row==1 && col==1)
                verb && println("SizeGen==1, middle")
                return false
            else
                verb && println("SizeGen==1, edge")
                return true
            end
        else ###aka SizeGen>1
            ###First we determin which third this is in (but make smaller by one)
            NextSize=get_lattice_size(SizeGen-1,"SNS")
            rowpart=div(row+1,NextSize)
            colpart=div(col+1,NextSize)
            verb && println("(rowpart,colpart)=($rowpart,$colpart)")
            if !(rowpart==1 && colpart==1)
                rowdiff=rem(row,NextSize-1)
                coldiff=rem(col,NextSize-1)
                verb && println("(rowdiff,coldiff)=($rowdiff,$coldiff)")
                return IsOnSNFractal(rowdiff,coldiff,SizeGen-1,FracGen-1,verb)
            else
                rowdiff=rem(row+1,NextSize)
                coldiff=rem(col+1,NextSize)
                if rowdiff == (NextSize-1) || coldiff == (NextSize-1) || rowdiff == 0 || coldiff == 0
                    verb && println("SizeGen>1, inner bundary")
                    return true
                else
                    verb && println("SizeGen>1, middle")
                    return false
                end
            end
        end
    end
end

#end
