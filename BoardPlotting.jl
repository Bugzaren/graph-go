
println("Including BoardPLotting.jl")

using PyCall
plt=pyimport("matplotlib.pyplot")
patch = pyimport("matplotlib.patches")
MplCollections = pyimport("matplotlib.collections")
ColorMaps = pyimport("matplotlib.cm")



function PlotStone(StoneID::Integer,BoardData::PyDict)
    do_PlotStone(StoneID::Integer,BoardData)
end

function PlotStone(StoneID::Integer,BoardData::Dict)
    do_PlotStone(StoneID::Integer,BoardData)
end
    
function do_PlotStone(StoneID::Integer,BoardData)
    ###Plot the new stone
    if StoneID!=0
        NodeStatus=BoardData["BoardState"][StoneID]
        IPM=BoardData["IPM"]
        (YPos,XPos)=IPM[:,StoneID]
        if NodeStatus==1
            circle=patch.Circle((XPos,YPos),.45,color="black",zorder=3)
        elseif NodeStatus==-1
            circle=patch.Circle((XPos,YPos),.45,color="grey",zorder=3)
        else
            throw(DomainError(NodeStatus,"NodeStatus is not 1 or -1"))
        end
        plt.gcf()["gca"]()["add_artist"](circle)
    end
    ####Plot the marker
    XLIM=plt.xlim()
    YLIM=plt.ylim()
    StoneType=BoardData["StoneType"]    
    if StoneType==1
        circle=patch.Circle((XLIM[2]-.5,YLIM[2]+.5),.45,
                            color="black",zorder=3,clip_on=false)
    elseif StoneType==-1
        circle=patch.Circle((XLIM[2]-.5,YLIM[2]+.5),.45,
                            color="grey",zorder=3,clip_on=false)
    end
    plt.gcf()."gca"()."add_artist"(circle)

end

function PlotBoard(BoardData)
    println("Plotting the Board")
    plt.clf() ###Clearing the figure ###FIXME Shuuld not be nececarry
    StoneType=BoardData["StoneType"]
    Board=BoardData["BoardState"]
    IPM=BoardData["IPM"]
    LinkMap=BoardData["LinkMap"]
    Prisoners=BoardData["Prisoners"]


    ###We assume a figure exists
    XLIM=(minimum(IPM[2,:])-.5,maximum(IPM[2,:])+.5)
    YLIM=(minimum(IPM[1,:])-.5,maximum(IPM[1,:])+.5)
    axes([.1,.1,.9,.8])
    println("Setting the colors and stuff.")
    plt.plot([])
    img = plt.imread("pic/wood_texture.jpg")
    #plt.gcf()."gca"()."set_facecolor"((.9,.3,.15))
    plt.gcf()."gca"().imshow(img,extent=[XLIM[1],XLIM[2],YLIM[1],YLIM[2]])
    plt.xlim(XLIM)
    plt.ylim(YLIM)
    plt.gcf()."gca"()."set_xticks"([])
    plt.gcf()."gca"()."set_yticks"([])
    ###Set the background color to brown-ish

    lc = MplCollections.LineCollection(LinkMap, colors="black", linewidths=2)
    plt.gcf()."gca"()."add_collection"(lc)
    
    PatchesEmpty=[]
    PatchesBlack=[]
    PatchesWhite=[]
    for Pos1 in 1:length(Board)
        NodeStatus=Board[Pos1]
        (YPos,XPos)=IPM[:,Pos1]
        if NodeStatus==0
            circle=patch.Circle((XPos,YPos),.1)
            push!(PatchesEmpty,circle)
        elseif NodeStatus==1
            circle=patch.Circle((XPos,YPos),.45)
            push!(PatchesBlack,circle)
        elseif NodeStatus==-1
            circle=patch.Circle((XPos,YPos),.45)
            push!(PatchesWhite,circle)
        end
    end
    PathCollecEmpty=MplCollections.PatchCollection(PatchesEmpty,zorder=3,
                                                   cmap=ColorMaps.binary)
    PathCollecBlack=MplCollections.PatchCollection(PatchesBlack,zorder=3,
                                                   cmap=ColorMaps.binary)
    PathCollecWhite=MplCollections.PatchCollection(PatchesWhite,zorder=3,
                                                   cmap=ColorMaps.binary)

    PathCollecEmpty."set_array"(fill(1,length(PatchesEmpty)))
    PathCollecBlack."set_array"(fill(1,length(PatchesBlack)))
    PathCollecWhite."set_array"(fill(0.5,length(PatchesWhite)))

    PathCollecEmpty."set_clim"((0,1))
    PathCollecBlack."set_clim"((0,1))
    PathCollecWhite."set_clim"((0,1))
    

    plt.gcf()."gca"()."add_collection"(PathCollecEmpty)
    plt.gcf()."gca"()."add_collection"(PathCollecBlack)
    plt.gcf()."gca"()."add_collection"(PathCollecWhite)
        
    if StoneType==1
        circle=patch.Circle((XLIM[2]-.5,YLIM[2]+.5),.45,
                            color="black",zorder=3,clip_on=false)
    elseif StoneType==-1
        circle=patch.Circle((XLIM[2]-.5,YLIM[2]+.5),.45,
                            color="grey",zorder=3,clip_on=false)
    end
    plt.gcf()."gca"()."add_artist"(circle)
    plt.title("White: "*string(Prisoners[1])*" "*
              "Black: "*string(Prisoners[2]),size=20)
    plt.show(block=false)
    println("Done printing the Board")
end



function PlotOwnedArea(BoardData)
    println("Plotting the owned Area")
    IPM=BoardData["IPM"]
    AreaOwner=BoardData["AreaOwner"]
    AreaDict=BoardData["AreaDict"]
    Prisoners=BoardData["Prisoners"]
    PatchesList=[]
    PatchesCol=fill(0.0,0)
    WhiteArea=0
    BlackArea=0
    for AreaKey in keys(AreaDict)
        LocalOwner=AreaOwner[AreaKey]
        if LocalOwner!=0
            if LocalOwner==1
                BlackArea+=length(AreaDict[AreaKey])
            elseif LocalOwner==-1
                WhiteArea+=length(AreaDict[AreaKey])
            end
            for ID in AreaDict[AreaKey]
                (YPos,XPos)=IPM[:,ID]
                if LocalOwner==1
                    circle=patch.Circle((XPos,YPos),.2,color="black",zorder=3)
                    push!(PatchesCol,1)
                elseif LocalOwner==-1
                    circle=patch.Circle((XPos,YPos),.2,color="grey",zorder=3)
                    push!(PatchesCol,.5)
                end
                push!(PatchesList,circle)
            end
        end        
    end

    PatchCollection=MplCollections.PatchCollection(PatchesList,zorder=3,
                                                  cmap=ColorMaps.binary)
    PatchCollection["set_array"](PatchesCol)
    PatchCollection["set_clim"]((0,1))
    gcf()["gca"]()["add_collection"](PatchCollection)


    title("White: "*string(Prisoners[1])*" ("*string(WhiteArea)*
          ") "*string(Prisoners[1]+WhiteArea)*
          " Black: "*string(Prisoners[2])*" ("*string(BlackArea)*
          ") "*string(Prisoners[2]+BlackArea)
          ,size=20)
    
    println("Done printing the Area")
end


println("Exiting  BoardPLotting.jl")
