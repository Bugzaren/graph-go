using PyCall
using Test

println("including PrintBoardActions.jl")


py"""
import numpy as np

class ClickCollector:
    def __init__(self,fig,BoardData):
        self.bd=BoardData
        self.cid = fig.canvas.mpl_connect("button_press_event",self)
    def __call__(self,event):
        EventData=(event.x,event.y,event.xdata,event.ydata)
        _,self.bd=self.bd["AddStonePos"](EventData[2],EventData[3],self.bd)
        return EventData

class ClickRemover:
    def __init__(self,fig,BoardData):
        self.bd=BoardData
        self.cid = fig.canvas.mpl_connect("button_press_event",self)
    def __call__(self,event):
        EventData=(event.x,event.y,event.xdata,event.ydata)
        _,self.bd=self.bd["RemoveStonePos"](EventData[2],EventData[3],self.bd)
        return EventData    


"""

#### ----------- COnnector functions ---------- ####
### A bunch of relay funcitons that shuffle data between the conncetor
### and the acutall functions 
function AddStoneIDConnector(StoneID,ConnectorID)
    BoardData=PyDict(ConnectorID."bd")
    _,BoardData=AddStoneID(StoneID,BoardData)
    ConnectorID."bd"=BoardData
end

function AddStonePosConnector(Position,ConnectorID)
    BoardData=PyDict(ConnectorID."bd")
    _,BoardData=AddStonePos(Position[1],Position[2],BoardData)
    ConnectorID."bd".=BoardData
end

function AddStonePosConnector(Position,ConnectorID)
    BoardData=PyDict(ConnectorID."bd")
    _,BoardData=RemoveStonePos(Position[1],Position[2],BoardData)
    ConnectorID."bd".=BoardData
end

function AddRandomStoneIDConnector(ConnectorID)
    BoardData=PyDict(ConnectorID."bd")
    _,BoardData=AddRandomStone(BoardData)
    ConnectorID."bd"=BoardData
end

function PassIDConnector(ConnectorID;PlayerInput=true)
    BoardData=PyDict(ConnectorID."bd")
    _,BoardData=Pass(BoardData;PlayerInput=true)
    ConnectorID."bd"=BoardData
end

function ToggleScoreIDConnector(ConnectorID)
    BoardData=PyDict(ConnectorID."bd")
    _,BoardData=ToggleScore(BoardData)
    ConnectorID."bd"=BoardData
end

function ToggleScore(BoardData)
    AlwaysScore=BoardData["AlwaysScore"]
    #Aprintln("AlwaysScore before set: $AlwaysScore")
    BoardData["AlwaysScore"]=!AlwaysScore
    println("Set AlwaysScore=",BoardData["AlwaysScore"])
    return true,BoardData
end



function AddRandomStone(BoardData;PlayerInput=true)    
    BoardState=BoardData["BoardState"]
    NumNodes=BoardData["NumNodes"]
    AvailableNodes=(1:NumNodes)[BoardState.==0]
    println("---...---...----...----....---...---...---...---")
    println("The available nodes are now",AvailableNodes)
    println("---...---...----...----....---...---...---...---")
    while length(AvailableNodes)>=1
        TryIDNum=rand(1:length(AvailableNodes))
        println("Choose ID ",TryIDNum,"= node ",AvailableNodes[TryIDNum])
        AllowedMove,BoardData=AddStoneID(AvailableNodes[TryIDNum],BoardData,
                                         PlayerInput=PlayerInput)
        if AllowedMove
            println("Sucess the random move was OK")
            ###Sucess,quit
            return true,BoardData
        else
            println("Faliure! the random move was not OK. Choose a new one")
            ###This was a scuicide stone..
            ##remove it form the list and try again..
            AvailableNodes=vcat(AvailableNodes[1:(TryIDNum-1)],
                                AvailableNodes[(TryIDNum+1):end])
            println("---...---...----...----....---...---...---...---")
            println("The available nodes are now",AvailableNodes)
            println("---...---...----...----....---...---...---...---")
        end
    end
    ### If we haven stopped now, we should pass, at least if we are the computer player
    if !PlayerInput
        println("Exausted lsit of possible moves. I'll just pass!")
        return Pass(BoardData,PlayerInput=PlayerInput)
    end
    println("Exausted list of possible moves. Let the player decide what goes next!")
    false, BoardData
end

function Pass(BoardData;PlayerInput=true)
    BoardData["StoneType"]*=-1
    PlotStone(0,BoardData)
    if PlayerInput && BoardData["SinglePlayer"]
        println("Add a random stone")
        return AddRandomStone(BoardData,PlayerInput=false)
    end
    println("Return controll")
    return true,BoardData
end

function GroupHasLiberties(IDList,NeigborList,BoardState)
    for ID in IDList
        if StoneHasLiberties(ID,NeigborList,BoardState)
            return true
        end
    end
    false
end


function StoneHasLiberties(ID,NeigborList,BoardState)
    FoundOponentLiberties=false
    for NID in NeigborList[ID]
        if BoardState[NID]==0
            return true
        end
    end
    return false
end

function AddStonePos(XPos::Nothing,YPos::Nothing,BoardData);
    false,BoardData end
function AddStonePos(XPos::Real,YPos::Nothing,BoardData);
    false,BoardData end
function AddStonePos(XPos::Nothing,YPos::Real,BoardData);
    false,BoardData end
function AddStonePos(XPos::Real,YPos::Real,BoardData)
    ####Give a float position, and lett the gaem figure out which stone is clostest
    
    IPM=BoardData["IPM"]
    ClosestStone= argmin((IPM[1,:] .- YPos).^2 .+ (IPM[2,:] .- XPos).^2)
    return AddStoneID(ClosestStone,BoardData)
end


function RemoveStonePos(XPos::Nothing,YPos::Nothing,BoardData);
    false,BoardData end
function RemoveStonePos(XPos::Real,YPos::Nothing,BoardData);
    false,BoardData end
function RemoveStonePos(XPos::Nothing,YPos::Real,BoardData);
    false,BoardData end
function RemoveStonePos(XPos::Real,YPos::Real,BoardData)
    ####Give a float position, and lett the gaem figure out which stone is clostest
    
    IPM=BoardData["IPM"]
    ClosestStone= argmin((IPM[1,:] .- YPos).^2 .+ (IPM[2,:] .- XPos).^2)
    return RemoveStoneID(ClosestStone,BoardData)
end



function RemoveStoneID(StoneID::Integer,BoardData)
    println("Removing Stone with ID ",StoneID)
    BoardState=BoardData["BoardState"]
    display(BoardData["BoardState"])

    ###Check if there is a stone there
    if BoardState[StoneID]==0
        return false, BoardData
    else
        ####If threre is a stone, we kill the entire group###
        ClusterID=BoardData["ClusterID"]
        ClusterDict=BoardData["ClusterDict"]
        Prisoners=BoardData["Prisoners"]


        LocalClusterID=ClusterID[StoneID]
        LocalCLuster=ClusterDict[LocalClusterID]

        ###Add the stones as prinoners
        StoneType=BoardState[StoneID]
        ClusterID[LocalCLuster].=0
        BoardState[LocalCLuster].=0
        Prisoners[div(3-StoneType,2)]+=length(LocalCLuster)
        delete!(ClusterDict,LocalClusterID)

        
        #println("BoardState after weeding:",BoardState)
        BoardData["BoardState"]=BoardState
        BoardData["ClusterID"]=ClusterID
        BoardData["Prisoners"]=Prisoners
        BoardData["ClusterDict"]=ClusterDict

        
        display(BoardData["BoardState"])
        ScoreTheGame(BoardData)
        return true,BoardData
    end
end


function AddStoneID(StoneID::Integer,BoardData;PlayerInput=true)
    println("BoardData[\"SinglePlayer\"]=",BoardData["SinglePlayer"])
    MoveAllowed,BoardData=Do_AddStoneID(StoneID,BoardData)
    println("BoardData[\"SinglePlayer\"]=",BoardData["SinglePlayer"])

    #### If single player mode is active, let the computer to the next move
    println("Was the move alled? $MoveAllowed")
    if MoveAllowed && PlayerInput && BoardData["SinglePlayer"]
        println("Add a random stone")
        MoveAllowed,BoardData=AddRandomStone(BoardData,PlayerInput=false)
    end
    println("Return controll")
    return MoveAllowed,BoardData    
end

function Do_AddStoneID(StoneID::Integer,BoardData)
    check_consistency(BoardData)
    res,BoardData=Do_Do_AddStoneID(StoneID,BoardData)
    check_consistency(BoardData)
    return res,BoardData
    
end
function Do_Do_AddStoneID(StoneID::Integer,BoardData;Verbose=true)
    Verbose && println("....................................")
    Verbose && println("Adding Stone with ID ",StoneID)
    #println(PrintBoard(BoardData))
    BoardState=BoardData["BoardState"]
    
    ###Check if there is a stone there
    if BoardState[StoneID]!=0
        Verbose &&  println("Occupied")
        return false, BoardData
    else
        ####First let's check if this is next to a stone with the same color
        Verbose &&  println("getting the dictioneries")


        #### Get the relevant lists
        StoneType=BoardData["StoneType"]
        NeigborList=BoardData["NeigborList"]
        NIDList=NeigborList[StoneID]
        ClusterID=BoardData["ClusterID"]
        ClusterDict=BoardData["ClusterDict"]
        Prisoners=BoardData["Prisoners"]
        
        Verbose && println("Setting board state")
        
        ###For now we add the stone to the board
        BoardState[StoneID]=StoneType
        BoardData["BoardState"]=BoardState
        
        Verbose && println("Checking neigbours")
        
        ####Update the Lists
        MyCID=0
        HasClusterNeigbors=false
        Verbose && println("Ny neigbours are: $NIDList")
        for NID in NIDList
            Verbose && println("Checking neigbour: $NID")
            if BoardState[NID]==StoneType
                Verbose && println("Found friendly neigbour at ID=$NID")
                
                if !HasClusterNeigbors
                    Verbose && println("I'm not part of a cluster")
                    MyCID=ClusterID[NID]
                    ClusterID[StoneID]=MyCID
                    ClusterDict[MyCID]=push!(ClusterDict[MyCID],StoneID)
                else
                    Verbose && println("I'm part of a cluster already")
                    CID=ClusterID[NID]
                    if CID==MyCID
                        Verbose && println("Elements are in same group")
                    else
                        Verbose && println("Elements are in differnt  groups")
                        ClusterStones=ClusterDict[CID]
                        ##Make Thse stones part of the same cluster
                        ClusterID[ClusterStones].=MyCID
                        delete!(ClusterDict,CID)
                        ClusterDict[MyCID]=append!(ClusterDict[MyCID],ClusterStones)
                    end
                end
                HasClusterNeigbors=true
            else
                Verbose && println("No friendly neigbour at ID=$NID")
            end
        end
        
        #### My group had no neigbours. So i'll create this state
        if !HasClusterNeigbors
            NextClusterID=BoardData["NextClusterID"]
            MyCID=NextClusterID
            BoardData["NextClusterID"]+=1
            Verbose && println("Found no friendly neigbors: Using MyCID=$MyCID")
            ###Found no friendly neigbours
            ###Add a new group using next CID
            ClusterID[StoneID]=MyCID
            ClusterDict[MyCID]=[StoneID]
            ###Increase the cluster counter
        end
        Verbose && println("Check for liberties")


        BoardData["ClusterID"]=ClusterID
        BoardData["ClusterDict"]=ClusterDict
        BoardData["BoardState"]=BoardState
        BoardData["Prisoners"]=Prisoners
        check_consistency(BoardData)

        
        FoundLiberties=GroupHasLiberties(ClusterDict[MyCID],
                                         NeigborList,BoardState)

        Verbose && println("Try and kill opponent")
        
        KilledObjects,BoardData=KillNeigbours(StoneID,BoardData)

        println("----- After trying to kill ---")
        #println(PrintBoard(BoardData))
        
        if KilledObjects ###All is well there is nothing more to do
            Verbose && println("Manged to kill")
        else ###Nothing was killed, ergy my group should die
            Verbose && println("Nothing to kill")
            
            ###However, if my group was a only a single element
            ###The this move is not allowed.
            ###Wew therefore undo what was made earlier
            if FoundLiberties
                Verbose && println("Tthat's ok I still have liberties")
            else
                Verbose && println("Bad - I have no liberties")
                if !HasClusterNeigbors
                    Verbose && println("Also have no neigbours")
                    
                    ClusterDict=BoardData["ClusterDict"]
                    BoardState=BoardData["BoardState"]
                    ClusterID=BoardData["ClusterID"]
                    delete!(ClusterDict,MyCID)
                    ClusterID[StoneID]=0
                    BoardState[StoneID]=0
                    ###Increase the cluster counter
                    BoardData["NextClusterID"]-=1
                    BoardData["ClusterID"]=ClusterID
                    BoardData["ClusterDict"]=ClusterDict
                    BoardData["BoardState"]=BoardState
                    Verbose && println("Ergo i'm quite dead")
                    return false, BoardData
                else ###Heree i have neigbours, so we will commit suicide
                    Verbose &&  println("Killing my own group:",MyCID)
                    ClusterID=BoardData["ClusterID"]
                    ClusterDict=BoardData["ClusterDict"]
                    BoardState=BoardData["BoardState"]
                    Prisoners=BoardData["Prisoners"]
                    ClusterID[ClusterDict[MyCID]].=0
                    BoardState[ClusterDict[MyCID]].=0
                    Prisoners[div(3-StoneType,2)]+=length(ClusterDict[MyCID])
                    delete!(ClusterDict,MyCID)
                    KilledObjects=true
                    BoardData["ClusterID"]=ClusterID
                    BoardData["ClusterDict"]=ClusterDict
                    BoardData["BoardState"]=BoardState
                    BoardData["Prisoners"]=Prisoners

                end
            end
        end
BoardData["StoneType"]*=-1
push!(BoardData["History"],StoneID)

end
check_consistency(BoardData)
return true,BoardData
end

function PldPLotting()
    if BoardData["AlwaysScore"]
        Verbose &&  println("Print Score")
        
        #PlotBoard(BoardData)
        ScoreTheGame(BoardData)
    elseif KilledObjects
        Verbose &&  println("Killed stuff replot")
        
        PlotBoard(BoardData)
        
    else 
        Verbose &&  println("Added stone, plot that")
        
        #PlotBoard(BoardData)
        PlotStone(StoneID,BoardData)
    end
    Verbose &&  println("Printed now return")
end



function KillNeigbours(StoneID,BoardData)
    check_consistency(BoardData)
    KillList=Array{Int64,1}()
    BoardState=BoardData["BoardState"]
    StoneType=BoardState[StoneID]
    NeigborList=BoardData["NeigborList"]
    ClusterID=BoardData["ClusterID"]
    ClusterDict=BoardData["ClusterDict"]
    println("ClusterID:\n",ClusterID)
    println("BoardState:\n",BoardState)
    
    
    NIDList=NeigborList[StoneID]
    KilledObjects=false
    for NID in NIDList ###Chech if enigbours can be killed
        if BoardState[NID]==StoneType
            ###The clusters should be the same
            if ClusterID[StoneID]!=ClusterID[NID]
                println("StoneID=",StoneID)
                println("NID=",NID)
            end
            @test ClusterID[StoneID]==ClusterID[NID]

        elseif BoardState[NID]==-StoneType
            ###This is stones of the oposite color. Check if they have liberties
            OppoentClusterID=ClusterID[NID]
            println("OppoentClusterID:",OppoentClusterID)
            OppoenntStones=ClusterDict[OppoentClusterID]
            println("OppoenntStones:",OppoenntStones)

            OponentLiberties=GroupHasLiberties(OppoenntStones,
                                               NeigborList,BoardState)
            if !OponentLiberties
                push!(KillList,ClusterID[NID])
                FoundLiberties=true
            end
        end
    end
    Prisoners=BoardData["Prisoners"]
    println("KillList:",KillList)
    if length(KillList)!=0
        println("Before killing")
        #println(PrintBoard(BoardData))
        for KillGroup in unique(KillList)
            println("Killing group:",KillGroup)
            ClusterID[ClusterDict[KillGroup]].=0
            BoardState[ClusterDict[KillGroup]].=0
            Prisoners[div(3+StoneType,2)]+=length(ClusterDict[KillGroup])
            delete!(ClusterDict,KillGroup)
            KilledObjects=true                
        end
        println("Before saving data")
        #println(PrintBoard(BoardData))
        BoardData["ClusterID"]=ClusterID
        BoardData["BoardState"]=BoardState
        BoardData["Prisoners"]=Prisoners
        BoardData["ClusterDict"]=ClusterDict
        println("After saving data")
        #println(PrintBoard(BoardData))
    end
    return KilledObjects, BoardData
end


function ScoreTheGame(BoardData)
    check_consistency(BoardData)
    
    ####Start by fining all groups of unoccupied partilces
    ClusterDict=BoardData["ClusterDict"]
    ##AreaDict=BoardData["AreaDict"]
    ###We reset the are dict
    AreaDict=Dict{Integer,Array{Int64,1}}()
    ###We reset the cluster IDs of the areas
    ClusterID=BoardData["ClusterID"]
    ClusterID[ClusterID .< 0] .= 0
    BoardData["ClusterID"]=ClusterID
    BoardState=BoardData["BoardState"]
    NeigborList=BoardData["NeigborList"]

    
    for StoneID in 1:length(BoardState)
        ###The board positions is unnocupied
        if BoardState[StoneID]==0
            ###Check if my ID is already set
            if ClusterID[StoneID] == 0
                HasClusterAssignment=false
            else
                HasClusterAssignment=true
                MyCID=ClusterID[StoneID]
            end
            NIDList=NeigborList[StoneID]
            for NID in NIDList
                ###Found an empty neihbour site
                if BoardState[NID]==0                    
                    ### Is this neigbour part of an area section?
                    if ClusterID[NID]==0
                        ####This is not accounted for yet...
                        ###Ignore it for now
                    else
                        ###This site is part of a cluster
                        if !HasClusterAssignment
                            ####My site is not part of a cluster
                            ####Add if to this cluster
                            MyCID=ClusterID[NID]
                            ClusterID[StoneID]=MyCID
                            AreaDict[MyCID]=push!(AreaDict[MyCID],StoneID)
                            ###Now i am part of a cluster
                            HasClusterAssignment=true
                            BoardData["AreaDict"]=AreaDict
                            BoardData["ClusterID"]=ClusterID
                        else ###ID already is part of a cluster
                            NClusterID=ClusterID[NID]
                            if NClusterID==MyCID
                                #println("Elements are in same group")
                                ###Do nothing
                            else
                                #println("Sites are in differnt groups")
                                ###Merge the groups
                                ClusterStones=AreaDict[NClusterID]
                                ##Make Thse stones part of the same cluster
                                ClusterID[ClusterStones].=MyCID
                                delete!(AreaDict,NClusterID)
                                AreaDict[MyCID]=append!(AreaDict[MyCID],ClusterStones)
                                BoardData["AreaDict"]=AreaDict
                                BoardData["ClusterID"]=ClusterID
                            end                                
                        end
                    end
                end
                ###Look for next stone
            end
            #### If the stone does not have a cluster ID
            #### Make one
            if !HasClusterAssignment
                MyCID=BoardData["NextAreaID"]
                ClusterID[StoneID]=MyCID
                AreaDict[MyCID]=[StoneID]
                BoardData["ClusterID"]=ClusterID
                BoardData["AreaDict"]=AreaDict
                
                ###Increase the cluster counter
                BoardData["NextAreaID"]=MyCID-1
            end
        end
    end
    ####  Now we want to do the scoring
    ###For this we loop over all the Area keys in the Area dictionary
    AreaOwner=Dict{Integer,Int64}()
    for AreaKey in keys(AreaDict)
        AreaOwner[AreaKey]=get_area_owner(AreaKey,AreaDict,NeigborList,BoardState)
    end
    BoardData["AreaOwner"]=AreaOwner
    #println("BoardData:")
    #display(BoardData)

    ###Reset cluster ownership
    ClusterID=BoardData["ClusterID"]
    ClusterID[ClusterID .< 0] .= 0
    BoardData["ClusterID"]=ClusterID

    return BoardData 
end



function get_area_owner(AreaKey,AreaDict,NeigborList,BoardState)
    AreaOwner=0
    #println("AreaKey=$AreaKey")
    ###Loop over all the area pieces
    for AreaID in AreaDict[AreaKey]
        ###Loop over the neigbours and check what state they are in
        for NID in NeigborList[AreaID]
            NeigborState=BoardState[NID]
            if NeigborState==0
                ##This is empy. Do nothing
            else
                ###If we do not have an owner yet, set that
                if AreaOwner==0
                    AreaOwner=NeigborState
                elseif NeigborState == AreaOwner
                    ####Do nothing, this is fine
                else
                    ###The owners are clashing. Ergo, no, one owns this
                    ###We can also break here
                    AreaOwner=0
                    return AreaOwner 
                end
            end
        end
    end
    return AreaOwner 
end



function check_consistency(BoardData;Verbose=true)
    Verbose && println("....................................")
    Verbose && println("Checking consistency")
    #println(PrintBoard(BoardData))
    ClusterID=BoardData["ClusterID"]
    ClusterDict=BoardData["ClusterDict"]
    StoneType=BoardData["StoneType"]
    BoardState=BoardData["BoardState"]
    Nodes=length(BoardState)

    ###Loop over the boar positons and malke sure that the clusters
    ###are consistent with the board positions
    for ID in 1:Nodes
        if BoardState[ID]!=0 ###Check the clusters
            CID=ClusterID[ID] ##Extract the CID
            if CID==0
                throw(DomainError(PrintBoard(BoardData),"The element $ID is recorded in one the board"*
                      " but does not have an assigned cluster"))
            end
            ClusterElems=ClusterDict[CID] ###Get the elements of the cluster
            ###
            if length(findall(ClusterElems .== ID))!=1
                if length(findall(ClusterElems .== ID))==0
                    throw(DomainError(PrintBoard(BoardData),"The element $ID is recorded in one the board"*
                          " but does not exist in the cluster dictionary"))
                else
                    throw(DomainError(PrintBoard(BoardData),"The element $ID is recorded in one the board"*
                                      " and is recorded several times in the cluster dictionary"))

                end
            end
        end
    end
    #### Now check the cluster ID:s and check the revese that the bard state is consistend
    for ID in 1:Nodes
        CID=ClusterID[ID] ##Extract the CID
        if CID!=0
            ClusterElems=ClusterDict[CID] ###Get the elements of the cluster
            GroupState=nothing
            for ID in ClusterElems
                NoteState=BoardState[ID]
                if NoteState==0
                    throw(DomainError(PrintBoard(BoardData),"The element $ID is recorded in the $CID cluster dictionary. "*
                          "But it is not recorded on the board"))
                elseif GroupState==nothing
                    GroupState=NoteState ##This is the color of the group
                elseif GroupState!=NoteState
                    throw(DomainError(PrintBoard(BoardData),"The $CID cluster dictionary, has elements. "*
                          "that are recorded with different colors in the board state"))
                end
            end
        end
    end
end



function PrintBoard(BoardData)
    ClusterID=BoardData["ClusterID"]
    ClusterDict=BoardData["ClusterDict"]
    BoardState=BoardData["BoardState"]
    Nodes=length(BoardState)

    ClusterIDString="ClusterID:\n"
    BoardStateString="BoardState:\n"
    
    for ID in 1:Nodes
        ClusterIDString*=(string(ClusterID[ID])*" ")
        BoardStateString*=(string(BoardState[ID])*" ")
    end
    ClusterDictString="ClusterDict:\n"
    for Key in keys(ClusterDict)
        ClusterDictString*=(string(Key)*":"*string(ClusterDict[Key])*"\n")
    end

    return "\n"*ClusterIDString*"\n"*BoardStateString*"\n"*ClusterDictString
end
    
println("Exiting PrintBoardActions.jl")
